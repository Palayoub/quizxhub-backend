FROM node:10.9.0-stretch

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY package.json /usr/src/app

RUN npm install yarn nodemon -g

RUN npm install

COPY . /usr/src/app

EXPOSE 4000

RUN NODE_PATH=src DEBUG=app

CMD ["node","src/app.js"]
