/**
 * Main application file
 */
const config = require("config/index");
const routes = require("routes/index");
const express = require("express");
const morgan = require("morgan");
const preTest = require("./test");
const app = express();
//const db = require("./db");

//Initialize DB
//db.init();
//db.initMysql();
//APP PORT
app.set("port", process.env.PORT || 4000);

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

//Morgan middleware
app.use(morgan("tiny"));
//Out-of-the-box bodyparser middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//APP ROUTES
routes(app);

//@TODO, get config variable from config dir
app.listen(app.get("port"), config.backendURL, function() {
  console.log("Application name :", config.appName);
  console.log("Environment\t :", "DEV");
  console.log("Port\t\t :", app.get("port"));
});

preTest();
