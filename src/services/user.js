const debug = require("debug")("app");
const mongoose = require("mongoose");
const UserModel = require("domains/User");
const fs = require("fs");

function saveUsers(request, response) {
  const email = request.body.email;
  const data = request.body.data;
  if (!email || !data) {
    response.status(400).send({ data: "Fail" });
    return;
  }
  const score = data.score;
  //const answers = JSON.stringify(data.answers);
  fs.appendFile("emails.txt", `${email}:${score}\n`, function(err) {
    if (err) throw err;
    console.log("Saved!");
    response.status(200).send({ data: "Success" });
  });
}

function listUsers(request, response) {
  fs.readFile("emails.txt", "utf8", function(err, contents) {
    response.status(200).send(contents);
  });
}

module.exports = {
  listUsers,
  saveUsers
};
