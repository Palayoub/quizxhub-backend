function BaseResponse(
  message = 'ACCEPT',
  responseCode = '000',
  status = 'OK',
  timestamp = new Date()
) {
  return {
    message: message,
    responseCode: responseCode,
    status: status,
    timestamp: timestamp
  };
}

module.exports = BaseResponse;
