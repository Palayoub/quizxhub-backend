const express = require("express");
const services = require("services/user.js");
const router = express.Router();

//router.get("/", services.listUsers);
router.post("/", services.saveUsers);
router.get("/", services.listUsers);

module.exports = router;
