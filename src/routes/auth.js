const express = require("express");
const services = require("services/auth");
const router = express.Router();

router.post("/signin", services.signin);
router.post("/signup", services.signup);

module.exports = router;
