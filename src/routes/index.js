const express = require("express");
const usersRoute = require("./user");
const authRoute = require("./auth");
const debug = require("debug")("app");

const router = express.Router();

//Console path and timestamp of every request
function middlewareTrack(app) {
  app.use("/", (req, res, next) => {
    console.log("\nPath : ", req.originalUrl);
    console.log("Time : ", Date.now());
    debug("Debug app message");
    next();
  });
}

function routes(app) {
  //Root route
  app.get("/", (req, res) => {
    res.send("Backend started, please use /api/*");
  });

  //middlewareTrack(app);

  //Global routes of our app
  app.use("/api/users", usersRoute);
  app.use("/api/auth", authRoute);
}

module.exports = routes;
